#!/bin/bash
for sysdevpath in $(find /sys/bus/usb/devices/usb*/ -name dev | grep 'ttyACM'); do
(
        syspath="${sysdevpath%/dev}"
        devname="$(udevadm info -q name -p $syspath)"
        [[ "$devname" == "bus/"* ]] && exit
        eval "$(udevadm info -q property --export -p $syspath)"
        [[ -z "$ID_SERIAL" ]] && exit
        if [[ "$ID_USB_INTERFACE_NUM" = "00" ]]; then
                echo "/dev/$devname - $ID_SERIAL_SHORT - $ID_USB_INTERFACE_NUM"
        fi
)
done
