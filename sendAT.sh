#!/bin/bash
function findPort {

	for sysdevpath in $(find /sys/bus/usb/devices/usb*/ -name dev | grep 'ttyACM'); do
	(
		syspath="${sysdevpath%/dev}"
		devname="$(udevadm info -q name -p $syspath)"
		[[ "$devname" == "bus/"* ]] && exit
		eval "$(udevadm info -q property --export -p $syspath)"
		[[ -z "$ID_SERIAL" ]] && exit
		if [[ "$ID_USB_INTERFACE_NUM" = "00" ]]; then
		       echo "/dev/$devname - $ID_SERIAL_SHORT - $ID_USB_INTERFACE_NUM"    
		fi
		
	)
	done
}

function printSomething {
	myPort = "/dev/ttyACM0"
	echo -n -e $myPort
}

function initRadio {
		
#	echo -n -e "AT+RS\n" 		> /dev/ttyACM0
#	sleep 0.5
#	echo "Module Reset"
	
	echo -n -e "AT+i 0\n" 		> /dev/ttyACM0
	sleep 0.1
	echo "Radio Initialized"
	
	echo -n -e "ATPFR=868000000\n" > /dev/ttyACM0
	echo "Communication frequency set to 868MHz"
	sleep 0.1
	
}

function sendData {
	echo -n -e "AT+TX Sent Data\n" > /dev/ttyACM0
	sleep 0.1
	echo "TX Success"
} 

function receiveData {
	echo -n -e "AT+RX"
}

function perTx {
	echo -n -e "ATPPP=100\n" > /dev/ttyACM0
	sleep 0.1
	echo "Ready for PER Tx"
	echo -n -e "ATPTM=3\n" > /dev/ttyACM0
}

function perRx {
	echo -n -e "ATPPP=100\n" > /dev/ttyACM0
	sleep 0.1
	echo "Ready for PER Rx"
	echo -n -e "ATPTM=4\n" > /dev/ttyACM0
}


findPort
initRadio
# sendData
perTx
echo -n -e "\nPER TX Done!" > /dev/ttyACM0

# perRx
# echo -n -e "PER RX Done!\n" > /dev/ttyACM0




